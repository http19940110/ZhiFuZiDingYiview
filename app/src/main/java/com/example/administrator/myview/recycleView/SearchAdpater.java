package com.example.administrator.myview.recycleView;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.administrator.myview.R;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Administrator on 2017/11/18.
 */

public class SearchAdpater extends RecyclerView.Adapter {

    List<String> list;
    private Context context;
    public SearchAdpater(List<String> list,Context context) {
        this.list=list;
        this.context=context;
    }

    @SuppressLint("InflateParams")
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new MyViewHolder(LayoutInflater.from(context).inflate(R.layout.search_recycle_item,null));
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        MyViewHolder myViewHolder=(MyViewHolder)holder;
        myViewHolder.title.setText(list.get(position));
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

   class MyViewHolder extends RecyclerView.ViewHolder{
        @BindView(R.id.title)
       TextView title;
       public MyViewHolder(View itemView) {
           super(itemView);
           ButterKnife.bind(this,itemView);//提交
       }
   }
}
