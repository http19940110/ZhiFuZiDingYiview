package com.example.administrator.myview.wxapi;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Message;
import android.util.Log;

import com.example.administrator.myview.BuildConfig;
import com.example.administrator.myview.MainActivity;
import com.example.administrator.myview.MyAppLication;
import com.tencent.mm.opensdk.constants.ConstantsAPI;
import com.tencent.mm.opensdk.modelbase.BaseReq;
import com.tencent.mm.opensdk.modelbase.BaseResp;
import com.tencent.mm.opensdk.openapi.IWXAPI;
import com.tencent.mm.opensdk.openapi.IWXAPIEventHandler;
import com.tencent.mm.opensdk.openapi.WXAPIFactory;


public class WXPayEntryActivity extends Activity implements IWXAPIEventHandler {
	
    private IWXAPI api;

    private static final String TAG = "MicroMsg.WXPayEntryActivity";
    

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
       // setContentView(R.layout.activity_wxpay_result);
    	api = WXAPIFactory.createWXAPI(this, "wx17e52c578aea5ff1");
        api.handleIntent(getIntent(), this);
    }

	@Override
	protected void onNewIntent(Intent intent) {
		super.onNewIntent(intent);
		setIntent(intent);
        api.handleIntent(intent, this);
	}

	@SuppressLint("LongLogTag")
	@Override
	public void onReq(BaseReq req) {
		if (BuildConfig.DEBUG) Log.d(TAG, "onReq, errCode = " + req.openId);
	}

	@SuppressLint("LongLogTag")
	@Override
	public void onResp(BaseResp resp) {
		if (BuildConfig.DEBUG) Log.d(TAG, "onResp, errCode = " + resp.errCode);
        MyAppLication mAPP = (MyAppLication) getApplication();
		
		//微信支付
		if (resp.getType() == ConstantsAPI.COMMAND_PAY_BY_WX) {
			MainActivity.WxPayCallbackHandler handler =mAPP.getWxPayCallbackHandler() ;	//回调
            //获取到handler对象发送消息
			if (handler != null) {
				Message message = handler.obtainMessage();
		        Bundle b=new Bundle();
		        message.what = 1;
		        b.putInt("retCode", resp.errCode);	//0：成功；-1：错误；-2：取消；
		        b.putString("retMsg", resp.errStr); //错误消息
		        message.setData(b);
		        handler.sendMessage(message);
			}
		}
    	finish();
	}
	
}
