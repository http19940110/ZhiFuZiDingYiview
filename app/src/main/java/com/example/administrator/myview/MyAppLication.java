package com.example.administrator.myview;

import android.app.Application;

/**
 * keystore
 * Created by Administrator on 2017/10/30.
 */

public class MyAppLication extends Application {
    private MainActivity.WxPayCallbackHandler handlerWxPayCallBack = null;			// 共享变量-微信支付回调句柄
    @Override
    public void onCreate() {
        super.onCreate();
    }

    //这样是好多地方都要用到的时候

    // 设置Handler回调 - 微信支付
    public void setWxPayCallbackHandler(MainActivity.WxPayCallbackHandler handler) {
        this.handlerWxPayCallBack = handler;
    }

    // 获取Handler回调 - 微信支付
    public MainActivity.WxPayCallbackHandler getWxPayCallbackHandler() {
        return handlerWxPayCallBack;
    }

}
