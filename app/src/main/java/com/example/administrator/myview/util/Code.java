package com.example.administrator.myview.util;

import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;

import java.util.Random;

//生成4位随机验证码
public class Code {
     
	private static final char[] CHARS = {
		'0','1','2', '3', '4', '5', '6', '7', '8', '9',
		'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'j', 'k', 'l', 'm', 
		'n', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z',
	};
	private static final int[] BGS ={
		Color.rgb(230, 251, 236)  //淡绿色背景
	};
	
	//默认设置
	private static final int DEFAULT_CODE_LENGTH = 4;	  					//4个字符
	private static final int DEFAULT_FONT_SIZE = 40;	  					//画笔大小
	private static final int DEFAULT_LINE_NUMBER = 3;	 			    	//干扰线
	private static final int DEFAULT_WIDTH = 120, DEFAULT_HEIGHT = 84;  	//验证码位图 宽 高
	private static final int BASE_PADDING_LEFT = 1, RANGE_PADDING_LEFT = 2; //字符之间左间距
	private static final int BASE_PADDING_TOP = 50, RANGE_PADDING_TOP = 20;  //字符基准线设置
	
	//canvas width and height
	private int width = DEFAULT_WIDTH, height = DEFAULT_HEIGHT;

	//random word space and pading_top
	private int base_padding_left = BASE_PADDING_LEFT, range_padding_left = RANGE_PADDING_LEFT,
			base_padding_top = BASE_PADDING_TOP, range_padding_top = RANGE_PADDING_TOP;

	//number of chars, lines; font size
	private int codeLength = DEFAULT_CODE_LENGTH, line_number = DEFAULT_LINE_NUMBER, font_size = DEFAULT_FONT_SIZE;
	
	//variables
	private String code;
	private int padding_left, padding_top;
	private Random random = new Random();
	
	private static Code bmpCode;
	
	public static Code getInstance() {
		if(bmpCode == null)
			bmpCode = new Code();
		return bmpCode;
	}
	
	
	//验证码图
	public Bitmap createBitmap() {
		padding_left = 0;
		code = createCode();
		
		Bitmap bp = Bitmap.createBitmap(width, height, Config.ARGB_8888);
		Canvas c = new Canvas(bp);
		c.drawColor(BGS[random.nextInt(BGS.length)]); 
		Paint paint = new Paint();
		paint.setTextSize(font_size);
		
		int PreLeft = 5; //上个字符左边距 
		for (int i = 0; i < code.length(); i++) {   
			randomTextStyle(paint);
			randomPadding();
			c.drawText(code.charAt(i) + "",PreLeft+padding_left, padding_top, paint);
			PreLeft +=padding_left+paint.measureText(""+code.charAt(i));
		}

		for (int i = 0; i < line_number; i++) {
			drawLine(c, paint); 
		}
		
		c.save( Canvas.ALL_SAVE_FLAG );//保存  
		c.restore();//
		return bp;
	}
	
	public String getCode() {
		return code;
	}
	
	//产生随机字符串
	private String createCode() {
		StringBuilder buffer = new StringBuilder();
		for (int i = 0; i < codeLength; i++) {
			buffer.append(CHARS[random.nextInt(CHARS.length)]);
		}
		return buffer.toString();
	}
	
	//绘制干扰线
	private void drawLine(Canvas canvas, Paint paint) {
		int color = randomColor();
		int startX = random.nextInt(width);
		int startY = random.nextInt(height);
		int stopX = random.nextInt(width);
		int stopY = random.nextInt(height);
		paint.setStrokeWidth(1);
		paint.setColor(color);
		canvas.drawLine(startX, startY, stopX, stopY, paint);
	}
	
	private int randomColor() {
		return randomColor(1);
	}
   
	private int randomColor(int rate) {
		int red = random.nextInt(256) / rate;
		int green = random.nextInt(256) / rate;
		int blue = random.nextInt(256) / rate;
		return Color.rgb(red, green, blue);
	}
	
	//设置倾斜度
	private void randomTextStyle(Paint paint) {
		int color = randomColor();
		paint.setColor(color);
		paint.setFakeBoldText(random.nextBoolean());  //true为粗体，false为非粗体
		float skewX = random.nextInt(5) / 4;
		skewX = random.nextBoolean() ? skewX : -skewX;
		paint.setTextSkewX(skewX/3); //float类型参数，负数表示右斜，整数左斜

	}
	
	private void randomPadding() {
		padding_left += base_padding_left + random.nextInt(range_padding_left);
		padding_top = base_padding_top + random.nextInt(range_padding_top);
	}


}
