package com.example.administrator.myview.voice;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.example.administrator.myview.R;

import java.util.ArrayList;
import java.util.List;

/**
 * 仿微信语音聊天 播放语音报错未解决
 * Created by Administrator on 2017/11/14.
 */

@SuppressLint("Registered")
public class RecorderActivity extends Activity {
    private ListView mListView;

    private ArrayAdapter<Recorder> mAdapter;
    private List<Recorder> mData = new ArrayList<>();//集合

//    private View animView;//动画

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.recoder_main);

        mListView = (ListView) findViewById(R.id.listView);
        AudioRecorderButton mAudioRecorderButton = (AudioRecorderButton) findViewById(R.id.id_recorder_button);


        //录音完成监听
        mAudioRecorderButton.setAudioFinishRecorderListener(new AudioRecorderButton.AudioFinishRecorderListener() {

            public void onFinish(float seconds, String filePath) {
                Toast.makeText(RecorderActivity.this,"录制成功",Toast.LENGTH_LONG).show();
                Recorder recorder = new Recorder(seconds, filePath);//实体类
                mData.add(recorder);
                mAdapter.notifyDataSetChanged(); //通知更新的内容
                mListView.setSelection(mData.size() - 1); //将lisView设置为最后一个
            }
        });

        mAdapter = new RecorderAdapter(this, mData);
        mListView.setAdapter(mAdapter);

        //listView的item点击事件  播放录音
        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            public void onItemClick(AdapterView<?> arg0, View view, int position, long id) {
                // 播放动画（帧动画）
//                if (animView != null) {
//                    //animView.setBackgroundResource(R.drawable.adj);
//                    animView = null;
//                }
//                animView = view.findViewById(R.id.id_recoder_anim);
//                animView.setBackgroundResource(R.drawable.play_anim);
//                AnimationDrawable animation = (AnimationDrawable) animView.getBackground();
//                animation.start();

                // 播放录音
                MediaManager.playSound(mData.get(position).filePath,new MediaPlayer.OnCompletionListener() {

                    public void onCompletion(MediaPlayer mp) {
                      //  animView.setBackgroundResource(R.drawable.adj);
                        Toast.makeText(RecorderActivity.this,"播放完成",Toast.LENGTH_LONG).show();
                    }
                });
            }
        });
    }

    @Override
    protected void onPause() {
        super.onPause();
        MediaManager.pause();
    }

    @Override
    protected void onResume() {
        super.onResume();
        MediaManager.resume();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        MediaManager.release();
    }

    //数据类
    class Recorder {
        float time;
        String filePath;

         Recorder(float time, String filePath) {
            super();
            this.time = time;
            this.filePath = filePath;
        }

        public float getTime() {
            return time;
        }

        public void setTime(float time) {
            this.time = time;
        }

        public String getFilePath() {
            return filePath;
        }

        public void setFilePath(String filePath) {
            this.filePath = filePath;
        }
    }
}
