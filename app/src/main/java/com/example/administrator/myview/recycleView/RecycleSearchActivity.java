package com.example.administrator.myview.recycleView;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.example.administrator.myview.R;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * 带搜索框的列表 滑动到底部的时候可以启动动画效果
 * Created by Administrator on 2017/11/18.
 */

public class RecycleSearchActivity extends Activity {

    @BindView(R.id.rv)
    RecyclerView mRecyclerView;
    @BindView(R.id.search)
    ViewUpSearch mViewUpSearch;//悬浮搜索框
    @BindView(R.id.PullRecyclerViewGroup)
    PullRecyclerViewGroup mPullRecyclerViewGroup;

    SearchAdpater searchAdpater;
    List<String> list=new ArrayList<>();
    private boolean isExpend = true;//标记悬浮框是否打开

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.search_recycle);
        ButterKnife.bind(this);
       mPullRecyclerViewGroup.setMoveViews(mViewUpSearch);//将搜索栏加入回弹效果 列表下拉回弹
        for (int i=0;i<20;i++){
            list.add("上滑动可以隐藏搜索框");
        }
        searchAdpater=new SearchAdpater(list,this);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mRecyclerView.setAdapter(searchAdpater);
        mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            /**
             *
             * @param recyclerView
             * @param dx 滑动X轴坐标
             * @param dy 滑动Y轴坐标 上滑为正 下拉为负
             */
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                //获得RecyclerView第一个可见的Item (这里头部的4个图片即为第一个Item-->0)
                int firstVisibleItemPosition = ((LinearLayoutManager) mRecyclerView.getLayoutManager()).findFirstVisibleItemPosition();
                if (firstVisibleItemPosition == 0 && dy > 0 && isExpend) {
                    mViewUpSearch.updateShow(!isExpend);
                    isExpend = false;
                } else if (firstVisibleItemPosition == 0 && dy < 0 && !isExpend) {
                    mViewUpSearch.updateShow(!isExpend);
                    isExpend = true;
                } else if (firstVisibleItemPosition == 1 && dy < 0 && isExpend) {
                    mViewUpSearch.updateShow(!isExpend);
                    isExpend = false;
                }
            }
        });

    }
}
