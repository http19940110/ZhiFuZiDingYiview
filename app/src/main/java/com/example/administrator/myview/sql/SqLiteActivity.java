package com.example.administrator.myview.sql;

import android.app.Activity;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.administrator.myview.R;

import butterknife.BindView;
import butterknife.ButterKnife;


/**
 * 基本的数据库 SQLite
 * Created by Administrator on 2017/11/27.
 */

public class SqLiteActivity extends Activity {
    SQLHelper sqlHelper;
    SQLiteDatabase db;

    @BindView(R.id.name)
    TextView userName;
    @BindView(R.id.paw)
    TextView userPassword;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.sql);
        ButterKnife.bind(this);
        //创建一个数据库
        sqlHelper=new SQLHelper(this);
        db = sqlHelper.getReadableDatabase();//只读数据库
        insert();
        delete();
        update();
        search();

        //对图片的操作
        ImageView showImv = (ImageView)findViewById(R.id.imageView);
        ImageSqLite imageSqlite=new ImageSqLite(this);
        imageSqlite.saveImage(R.drawable.balloon);//保存图片
        //读取图片
        byte[] imgData=imageSqlite.readImage();
        if (imgData!=null) {
            //将字节数组转化为位图
            Bitmap imageBitmap = BitmapFactory.decodeByteArray(imgData, 0, imgData.length);
            //将位图显示为图片
            showImv.setImageBitmap(imageBitmap);
        }else {
            showImv.setBackgroundResource(android.R.drawable.menuitem_background);
        }
    }


    //增加一个用户 数据库中添加一个用户
    private void insert() {
        //使用insert方法 user为数据库中的一个表名
        ContentValues cv = new ContentValues();//实例化一个ContentValues用来装载待插入的数据
        cv.put("username","Ma");//添加用户名
        cv.put("password","123456"); //添加密码
        db.insert("user",null,cv);//执行插入操作
        //使用execSQL方法
//        String sql = "insert into user(username,password) values ('Ma','123456')";//插入操作的SQL语句
//        db.execSQL(sql);//执行SQL语句
    }

    //删除用户
    private void delete() {
        String whereClause = "username=?";//删除的条件
        String[] whereArgs = {"Ma"};//删除的条件参数
        db.delete("user",whereClause,whereArgs);//执行删除 拼接成删除语句
        //第二种方法
//        String sqlDelete = "delete from user where username='Ma'";//删除操作的SQL语句
//        db.execSQL(sqlDelete);//执行删除操作
    }


    private void update() {
        //修改用户密码
        ContentValues cvNew = new ContentValues();//实例化ContentValues
        cvNew.put("password","iHatePopMusic");//添加要更改的字段及内容
        String whereClauseUpdate = "username=?";//修改条件
        String[] whereArgsUpdate = {"Ma"};//修改条件的参数
        db.update("user",cvNew,whereClauseUpdate,whereArgsUpdate);//执行修改
    }

    //查
    private void search() {
        /**
         * table：表名称
         colums：列名称数组
         selection：条件子句，相当于where
         selectionArgs：条件语句的参数数组
         groupBy：分组
         having：分组条件
         orderBy：排序类
         limit：分页查询的限制
         Cursor：返回值，相当于结果集ResultSet
         */
        Cursor c = db.query("user", new String[]{"username","password"},null,null,
                null,null,null);//查询并获得游标
        if(c.moveToFirst()){//判断游标是否为空
            for(int i=0;i<c.getCount();i++){
                c.move(i);//移动到指定记录
                String username = c.getString(c.getColumnIndex("username"));
                String password = c.getString(c.getColumnIndex("password"));
                //显示在textView上面 遗留没显示、、、
                userName.setText(username);
                userPassword.setText(password);
            }
        }

        //带参数查询 通过名字查询某个用户的密码信息
//        Cursor cursor = db.rawQuery("select * from user where username='?'",new String[]{"Ma"});
//        if(cursor.moveToFirst()) {
//            String password = c.getString(c.getColumnIndex("password"));
//        }
    }


}
