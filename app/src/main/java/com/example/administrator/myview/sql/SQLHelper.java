package com.example.administrator.myview.sql;

import android.content.Context;
import android.database.DatabaseErrorHandler;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * 辅助类 创建一个表
 * Created by Administrator on 2017/11/27.
 */

public class SQLHelper  extends SQLiteOpenHelper {


    private static SQLHelper helper;

    private static final String DB_NAME = "mydata.db"; //数据库名称
    private static final int version = 1; //数据库版本

    //单例
    public static SQLHelper getIns(Context context){
        if(helper == null){
            helper = new SQLHelper(context, "demo.db", null, 1);
        }
        return helper;
    }

    public SQLHelper(Context context) {
        super(context, DB_NAME, null, version);
    }

    // SQLiteOpenHelper的构造函数参数：
    // context：上下文环境
    // name：数据库名字
    // factory：游标工厂（可选）
    // version：数据库模型版本号
    public SQLHelper(Context context, String name,
                     SQLiteDatabase.CursorFactory factory, int version,
                     DatabaseErrorHandler errorHandler) {
        super(context, name, factory, version, errorHandler);
    }

    public SQLHelper(Context context, String name,
                     SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        //创建一个user表 存名字和密码信息
        String sql = "create table user(username varchar(20) not null , password varchar(60) not null );";
        db.execSQL(sql);
        //第二个表
        String createTb="CREATE TABLE Image (_id INTEGER PRIMARY KEY AUTOINCREMENT,name VARCHAR2,avatar BLOB)";
        db.execSQL(createTb);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
    }

    public void test(){
        helper.getReadableDatabase();
        helper.getWritableDatabase();
    }


    //创建或者打开一个只读数据库
    @Override
    public SQLiteDatabase getReadableDatabase() {
        return super.getReadableDatabase();
    }

    //创建或打开一个读写数据库
    @Override
    public SQLiteDatabase getWritableDatabase() {
        return super.getWritableDatabase();
    }
}
