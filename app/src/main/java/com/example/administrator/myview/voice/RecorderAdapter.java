package com.example.administrator.myview.voice;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.administrator.myview.R;

import java.util.List;

/**
 * 语音条目的适配器
 * Created by Administrator on 2017/11/14.
 */

@SuppressWarnings("ConstantConditions")
public class RecorderAdapter extends ArrayAdapter<RecorderActivity.Recorder> {
    private Context mContext;
    private List<RecorderActivity.Recorder> mDatas;

    private int mMinItemWidth; //最小的item宽度
    private int mMaxItemWidth; //最大的item宽度
    private LayoutInflater mInflater;

    public RecorderAdapter(Context context, List<RecorderActivity.Recorder> datas) {
        super(context, -1, datas);

        mContext = context;
        mDatas = datas;

        //获取屏幕的宽度
        WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        DisplayMetrics outMetrics = new DisplayMetrics();
        wm.getDefaultDisplay().getMetrics(outMetrics);
        mMaxItemWidth = (int) (outMetrics.widthPixels * 0.7f);
        mMinItemWidth = (int) (outMetrics.widthPixels * 0.15f);

        mInflater = LayoutInflater.from(context);
    }

    /**
     * 定义一个ViewHolder
     */
    private class ViewHolder {
        TextView seconds; // 时间
        View length; // 长度
    }

    @SuppressLint("SetTextI18n")
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder = null;
        if (convertView == null) {
         convertView = mInflater.inflate(R.layout.item_recoder, parent,false);
            holder = new ViewHolder();
            holder.seconds = (TextView) convertView.findViewById(R.id.id_recorder_time);
            holder.length = convertView.findViewById(R.id.length);

            convertView.setTag(holder);

        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        holder.seconds.setText(Math.round(getItem(position).time) + "\"");
        ViewGroup.LayoutParams lp = holder.length.getLayoutParams();
        lp.width = (int) (mMinItemWidth + (mMaxItemWidth / 60f)* getItem(position).time);
        return convertView;
    }
}
