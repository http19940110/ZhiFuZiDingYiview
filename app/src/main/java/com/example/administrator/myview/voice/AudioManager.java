package com.example.administrator.myview.voice;

import android.media.MediaRecorder;

import java.io.File;
import java.io.IOException;
import java.util.UUID;


/**
 * 录音管理类
  用来管理音频输出和音频来源
 * Created by Administrator on 2017/11/14.
 */

public class AudioManager {
    private MediaRecorder mMediaRecorder;//用于录制音频和视频的一个类
    private String mDir;
    private String mCurrentFilePath;

    private static AudioManager mInstance;//单例

    private boolean isPrepare;//是否准备好

    private AudioManager(String dir) {//构造
        mDir = dir;
    }

    //单例模式
    public static AudioManager getInstance(String dir) {
        if (mInstance == null) {
            synchronized (AudioManager.class) {
                if (mInstance == null) {
                    mInstance = new AudioManager(dir);
                }
            }
        }
        return mInstance;
    }

    /**
     * 使用接口 用于回调
     */
    public interface AudioStateListener {
        void wellPrepared();
    }
    public AudioStateListener mAudioStateListener;

    public void setOnAudioStateListener(AudioStateListener listener) {
        mAudioStateListener = listener;
    }


    // 去准备
    public void prepareAudio() {
        try {
            isPrepare = false;
            File dir = new File(mDir);//文件路径
            if (!dir.exists()) {
                dir.mkdirs();
            }
            String fileName = generateFileName();//随机生成文件名称
            File file = new File(dir, fileName);//文件路径+文件名字

            mCurrentFilePath =file.getAbsolutePath();//绝对路径

            mMediaRecorder = new MediaRecorder();
            // 设置输出文件
            mMediaRecorder.setOutputFile(dir.getAbsolutePath());
            // 设置MediaRecorder的音频源为麦克风
            mMediaRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
            //mMediaRecorder.setAudioSource(MediaRecorder.AudioSource.DEFAULT);
            // 设置音频格式
            mMediaRecorder.setOutputFormat(MediaRecorder.OutputFormat.RAW_AMR);
            // 设置音频编码
            mMediaRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);

            // 准备录音
            mMediaRecorder.prepare();
            // 开始
            mMediaRecorder.start();
            // 准备结束
            isPrepare = true;
            if (mAudioStateListener != null) {
                mAudioStateListener.wellPrepared();
            }

        } catch (IllegalStateException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    /**
     * 随机生成文件的名称
     * UUID.randomUUID().toString()是javaJDK提供的一个自动生成主键的方法。
     * UUID(Universally Unique Identifier)全局唯一标识符,是指在一台机器上生成的数字，
     * 它保证对在同一时空中的所有机器都是唯一的，是由一个十六位的数字组成,表现出来的形式。
     * 由以下几部分的组合：当前日期和时间(UUID的第一个部分与时间有关，如果你在生成一个UUID之后，
     * 过几秒又生成一个UUID，则第一个部分不同，其余相同)，时钟序列，全局唯一的IEEE机器识别号
     * （如果有网卡，从网卡获得，没有网卡以其他方式获得），UUID的唯一缺陷在于生成的结果串会比较长。
     */
    private String generateFileName() {
        return UUID.randomUUID().toString() + ".amr";
    }

    public int getVoiceLevel(int maxlevel) {
        if (isPrepare) {
            try {
                // mMediaRecorder.getMaxAmplitude() 1~32767
                return maxlevel * mMediaRecorder.getMaxAmplitude() / 32768 + 1;
            } catch (Exception e) {
            }
        }
        return 1;
    }

    /**
     * 释放资源
     */
    public void release() {
        mMediaRecorder.reset();
        mMediaRecorder = null;
    }

    /**
     * 取消录音
     */
    public void cancel() {
        release();
        if (mCurrentFilePath != null) {
            File file = new File(mCurrentFilePath);
            file.delete();
            mCurrentFilePath = null;
        }

    }

    public String getCurrentFilePath() {

        return mCurrentFilePath;
    }
}
