package com.example.administrator.myview.broadcast;

import android.content.ContentValues;
import android.content.Context;
import android.database.ContentObserver;
import android.database.Cursor;
import android.net.Uri;
import android.os.Handler;

/**
 *   短信的Uri共有一下几种：

 content://sms/inbox     收件箱
 content://sms/sent        已发送
 content://sms/draft        草稿
 content://sms/outbox    发件箱           (正在发送的信息)
 content://sms/failed      发送失败
 content://sms/queued  待发送列表  (比如开启飞行模式后，该短信就在待发送列表里)
 * Created by Administrator on 2017/11/23.
 */

public class SMSContentObserver extends ContentObserver {
    private Context context;
    private Handler handler;
    private int a = 0;

    public SMSContentObserver(Context context,Handler handler) {
        super(handler);
        this.context=context;
        this.handler=handler;
    }

    //处理回调
    @Override
    public void onChange(boolean selfChange) {
        super.onChange(selfChange);
        //指向 当ContentObserver所观察的Uri发生变化时，便会触发它
        Cursor cursor = context.getContentResolver().query(Uri.parse("content://sms/inbox"),
                null, null, null, null);
        ContentValues contentValues = new ContentValues();

        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            contentValues.put("person", cursor.getString(cursor.getColumnIndex("person")));
            contentValues.put("address", cursor.getString(cursor.getColumnIndex("address")));
            contentValues.put("body", cursor.getString(cursor.getColumnIndex("body")));
            contentValues.put("date", cursor.getString(cursor.getColumnIndex("date")));
            contentValues.put("type", cursor.getString(cursor.getColumnIndex("type")));
        }
        cursor.close();
        handler.obtainMessage(a, contentValues).sendToTarget();
    }

}
