package com.example.administrator.myview.broadcast;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;

/**
 * 把注册广播到基类里面 在需要的地方调用close发送广播 就会退出应用
 *
 *  Intent intent = new Intent();
 intent.setAction("finish");
 mContext.sendBroadcast(intent);//发送广播
 * Created by Administrator on 2017/12/6.
 */

public class BroadActivity extends Activity{



    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //动态注册
        IntentFilter filter = new IntentFilter();
        filter.addAction("finish");
        registerReceiver(mFinishReceiver, filter);//给activity创建广播接收器
    }
   //注册的接收器  创建接收器
    private BroadcastReceiver mFinishReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            if("finish".equals(intent.getAction())) {
                Log.e("#########", "I am " + getLocalClassName()
                        + ",now finishing myself...");
                finish();
            }
        }
    };
}
