package com.example.administrator.myview;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;

import com.example.administrator.myview.Base.BaseActivity;

/**
 * 继承第三方登录
 * 扣扣 微信  微博 等等登录功能的实现
 * Created by Administrator on 2017/11/16.
 */

public class ThirdLoginActivity extends BaseActivity{

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ceShi.setText("");
        initView();
    }
}
