package com.example.administrator.myview.mvvm.dataBinding;

import android.content.Intent;
import android.databinding.BaseObservable;
import android.databinding.Bindable;
import android.databinding.ObservableField;
import android.view.View;
import android.widget.Toast;

import com.example.administrator.myview.BR;
import com.example.administrator.myview.mvvm.dataBinding.main.MvvMActivity;
import com.example.administrator.myview.util.MyUtils;

/**
 * 实体类
 * 实现双向绑定 实体类修改
 * Created by Administrator on 2017/11/22.
 */

public class User  extends BaseObservable{
//    private String name;
//    private   String age;

    public ObservableField<String> name=new ObservableField<>();
    public ObservableField<String> age=new ObservableField<>();

    public void onItemClick(View view){
        Toast.makeText(view.getContext(),"点击成功",Toast.LENGTH_LONG).show();
        setName("我是改变后的");
        setAge("我是改变后的年龄");
//        Intent intent=new Intent(DataBindActivity,MvvMActivity.class);
//        view.getContext().startActivity(intent);
    }

    public User(String name, String age) {
        this.name.set(name);
        this.age.set(age);
    }

    @Bindable
    public String getName() {
        return name.get();
    }

    public void setName(String name) {
        this.name.set(name);
        notifyPropertyChanged(BR.name);//通知名字改变 双向绑定 点击事件改变值自动更新view
    }

    @Bindable
    public String getAge() {
        return age.get();
    }

    public void setAge(String age) {
        this.age.set(age);
        notifyPropertyChanged(BR.age);
    }
}
