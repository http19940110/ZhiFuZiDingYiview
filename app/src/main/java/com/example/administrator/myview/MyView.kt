package com.example.administrator.myview

import android.animation.ObjectAnimator
import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.graphics.RectF
import android.util.AttributeSet
import android.view.View



/**
 * 自定义view
 * Created by Administrator on 2017/10/26.
 */

class MyView : View {

    private lateinit var paint: Paint

    private lateinit var bitmap: Bitmap

    internal var objectAnimator: ObjectAnimator? = null

    constructor(context: Context) : super(context) {
        init()
    }

    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs) {
        init()
    }

    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        init()
    }

    private fun init() {
        paint = Paint()//初始化画笔
        paint.isAntiAlias = true//锯齿模糊
        paint.style = Paint.Style.STROKE//画笔空心
        paint.color = -0x1//画笔颜色
        paint.strokeWidth = 10f


        bitmap = BitmapFactory.decodeResource(resources, R.drawable.balloon)
    }

    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)
        canvas.drawLine(50f, 50f, 450f, 50f, paint)        //绘制直线
        canvas.drawRect(100f, 100f, 200f, 600f, paint)     //绘制矩形 根据坐标

        //圆角矩形
        @SuppressLint("DrawAllocation")
        val rectF = RectF(10f, 180f, 110f, 250f)
        canvas.drawRoundRect(rectF, 10f, 10f, paint)
        //坐标圆角矩形 此方法没有
        //  canvas.drawRoundRect(120, 180, 220, 250, 10, 10, paint);

        // 画圆形
        canvas.drawCircle(100f, 350f, 50f, paint)
        paint.style = Paint.Style.STROKE
        canvas.drawCircle(210f, 350f, 50f, paint)
        paint.style = Paint.Style.FILL_AND_STROKE
        canvas.drawCircle(320f, 350f, 50f, paint)
        paint.style = Paint.Style.FILL
        canvas.drawCircle(430f, 350f, 150f, paint) //圆心坐标xy 半径

        //画图
        canvas.drawBitmap(bitmap, 300f, 450f, paint)//中间是左上距离



       //画圆弧
        paint.isAntiAlias = true                       //设置画笔为无锯齿
        paint.color = Color.BLACK                    //设置画笔颜色
        canvas.drawColor(Color.WHITE)                  //白色背景
        paint.strokeWidth = 3.0.toFloat()              //线宽
        paint.style = Paint.Style.STROKE

        val oval = RectF()                     //RectF对象
        oval.left = 100f                              //左边
        oval.top = 100f                                   //上边
        oval.right = 400f                             //右边
        oval.bottom = 300f                                //下边
        canvas.drawArc(oval, 225F, 90F, false, paint)    //绘制圆弧

        //RectF oval=new RectF();                       //RectF对象
        oval.left = 100f                              //左边
        oval.top = 400f                                   //上边
        oval.right = 400f                             //右边
        oval.bottom = 700f                                //下边
        canvas.drawArc(oval, 200F, 135F, true, paint)    //绘制圆弧
    }
}
