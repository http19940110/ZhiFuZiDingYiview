package com.example.administrator.myview.http;

import android.util.Log;
import com.squareup.okhttp.Callback;
import com.squareup.okhttp.FormEncodingBuilder;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;

import java.io.IOException;

/**
 * OkHTTP的使用
 * 可以使用zhy的okHttpUtil对这个做了很好的封装 封装了返回不同的类型
 * Created by Administrator on 2017/12/5.
 */

public class OkHttp {

    //get请求
    private String get(String url) {
        OkHttpClient client = new OkHttpClient();
        Request request = new Request.Builder()
                .url(url)
                .build();
        Response response ;
        try {
            response = client.newCall(request).execute();//得到请求的返回值
            return response.body().string();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

     //       POST
  //  POST需要使用RequestBody对象，之后再构建Request对象时调用post函数将其传入即可
    //此方法也支持文件的操作

    private String post(String url) {
        OkHttpClient client = new OkHttpClient();

        RequestBody formBody = new FormEncodingBuilder()
                //添加参数
                .add("user", "Jurassic Park")
                .add("pass", "asasa")
                .add("time", "12132")
                .build();
        Request request = new Request.Builder()
                .url(url)
                .post(formBody)
                .build();
        Response response = null;
        try {
            response = client.newCall(request).execute();
            return response.body().string();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }


    //Person是一个实体类 okhttp支持gson
//    private Person gson(String url){
//        OkHttpClient client = new OkHttpClient();
//        Gson gson = new Gson();
//        Request request = new Request.Builder()
//                .url(url)
//                .build();
//        Response response = null;
//        try {
//            response = client.newCall(request).execute();
//            Person person = gson.fromJson(response.body().charStream(), Person.class);//Gson解析json 转化为实体类
//            return person;
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//        return null;
//    }




    //get请求的异步形式
    private void getAsync(String url) {
        OkHttpClient client = new OkHttpClient();
        Request request = new Request.Builder()
                .url(url)
                .build();
        Response response = null;

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Request request, IOException e) {

            }

            @Override
            public void onResponse(Response response) throws IOException {
                String result = response.body().string();
                //不能操作ui，回调依然在子线程
                Log.d("TAG", result);
            }
        });

    }
}
