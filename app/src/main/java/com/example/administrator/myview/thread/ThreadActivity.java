package com.example.administrator.myview.thread;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * 四种线程池
 * Created by Administrator on 2017/11/21.
 */

public class ThreadActivity  extends Activity{

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        firstThreadPool();
        twoThreadPool();
        threeThreadPool();
        fourThreadPool();

    }
    public static void main(String[] args) throws Exception {
        System.out.println("哈哈哈哈哈");
        throw new Exception("");
    }



    /**
     * newFixedThreadPool：创建固定大小的线程池，这样可以控制线程最大并发数，超出的线程会在队列中等待
     * 。如果线程池中的某个线程由于异常而结束，线程池则会再补充一条新线程。
     */
    private void firstThreadPool() {
        ExecutorService newFixedThreadPool = Executors.newFixedThreadPool(3);//参数为3就是只开三个线程
        for (int i = 0; i < 10; i++) {
            final int finalI = i;
            newFixedThreadPool.execute(new Runnable() {
                @Override
                public void run() {
                    try {
                        Thread.sleep(1000);
                        Log.d("tag1", "线程名字:" + Thread.currentThread().getName() + "i=" + finalI);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            });
        }
    }

    /**
     * 创建一个单线程的线程池，即这个线程池永远只有一个线程在运行，这样能保证所有任务按指定顺序来执行。
     * 如果这个线程异常结束，那么会有一个新的线程来替代它。
     */
    private void twoThreadPool() {
        ExecutorService newSingleThreadExecutor = Executors.newSingleThreadExecutor();
        //循环实现线程最大数量10
        for (int i = 0; i < 10; i++) {
            final int finalI = i;
            newSingleThreadExecutor.execute(new Runnable() {
                @Override
                public void run() {
                    try {
                        Thread.sleep(1000);
                        Log.d("tag2", "线程名字:" + Thread.currentThread().getName() + "i=" + finalI);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            });
        }
    }
    /**
     * newCachedThreadPool：创建带有缓存的线程池，在执行新的任务时，当线程池中有之前创建的可用线程就重用之前的线程，
     * 否则就新建一条线程。如果线程池中的线程在60秒未被使用，就会将它从线程池中移除。
     */
    private void threeThreadPool() {
        ExecutorService newCachedThreadPool = Executors.newCachedThreadPool();
        for (int i = 0; i < 10; i++) {
            final int finalI = i;
            newCachedThreadPool.execute(new Runnable() {
                @Override
                public void run() {
                    Log.d("tag3", "线程名字:" + Thread.currentThread().getName() + "  i=" + finalI);
                }
            });
        }
    }

    /**
     * newScheduledThreadPool：创建定时和周期性执行的线程池。
     */
    private void fourThreadPool() {
        ScheduledExecutorService newScheduledThreadPool = Executors.newScheduledThreadPool(3);
        /**
         * 延迟2秒执行任务
         */
        newScheduledThreadPool.schedule(new Runnable() {
            @Override
            public void run() {
                Log.d("", "线程名字:" + Thread.currentThread().getName() + "定时任务");
            }
        }, 2, TimeUnit.SECONDS);
        /**
         * 延迟1秒后每2秒执行一次任务
         */
        newScheduledThreadPool.scheduleAtFixedRate(new Runnable() {
            @Override
            public void run() {
                Log.d("", "线程名字:" + Thread.currentThread().getName() + "周期性任务");
            }
        }, 1, 2, TimeUnit.SECONDS);
    }




}
