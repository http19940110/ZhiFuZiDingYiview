package com.example.administrator.myview.mvvm.dataBinding;

import android.app.Activity;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;

import com.example.administrator.myview.R;
import com.example.administrator.myview.databinding.DataBindingBinding;

/**
 * v层是xml和activity
 * Created by Administrator on 2017/11/22.
 */

public class DataBindActivity extends Activity{

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
         super.onCreate(savedInstanceState);
          //单向绑定
          DataBindingBinding dataBindingBinding= DataBindingUtil.setContentView(this,R.layout.data_binding);
          User user=new User("马","23");//实例化数据类
          dataBindingBinding.setUser(user);//绑定数据到view
    }
}
