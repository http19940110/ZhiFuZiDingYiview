package com.example.administrator.myview.service;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.support.annotation.Nullable;

/**
 * 编写一个服务
 * Created by Administrator on 2017/12/8.
 */

public class MyService extends Service {

    private  boolean isStartService=false;

    //实例化内部类
    private LocalBinder localBinder=new LocalBinder();

    public void startMyService(){
        isStartService=true;
    }
    public void stopMyService(){
        isStartService=false;
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return localBinder;//绑定自定义的binder对象
    }

    //创建内部类继承Binder 通过binder来得到服务对象
    public class LocalBinder extends Binder{

        MyService getMyService(){
            return MyService.this;
        }

        boolean isStartMyService(){
            return isStartService;
        }
    }


    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }
}
